"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls import re_path
from django.urls import include
from web.views import account
from web.views import home
from web.views import project
from web.views import manage
from web.views import wiki
from web.views import file
from web.views import setting
from web.views import issues
from web.views import dashboard
from web.views import statistics

name = 'web'

urlpatterns = [
    re_path(r'^register/$', account.register, name='register'),
    re_path(r'^send_sms/$', account.send_sms, name='send_sms'),
    re_path(r'^login_sms/$', account.login_sms, name='login_sms'),
    re_path(r'^login/$', account.login, name='login'),
    re_path(r'^image/code/$', account.image_code, name='image_code'),
    re_path(r'^logout/$', account.logout, name='logout'),

    # 支付
    re_path(r'^price/$', home.price, name='price'),
    re_path(r'^payment/(?P<price_id>\d+)/$', home.payment, name='payment'),
    re_path(r'^pay_notify/$', home.pay_notify, name='pay_notify'),

    # 首页
    re_path(r'^index/$', home.index, name='index'),

    # 项目列表
    re_path(r'^project/', include([
        re_path(r'^list/$', project.project_list, name='project_list'),
        re_path(r'^star/(?P<project_type>\w+)/(?P<project_id>\d+)/$', project.project_star, name='project_star'),
        re_path(r'^unstar/(?P<project_type>\w+)/(?P<project_id>\d+)/$', project.project_unstar, name='project_unstar'),
    ], None), None),

    # 项目管理
    re_path(r'^manage/(?P<project_id>\d+)/', include([
        re_path(r'^dashboard/$', dashboard.dashboard, name='dashboard'),

        re_path(r'^wiki/$', wiki.wiki, name='wiki'),
        re_path(r'^wiki/add/$', wiki.wiki_add, name='wiki_add'),
        re_path(r'^wiki/edit/(?P<wiki_id>\d+)/$', wiki.wiki_edit, name='wiki_edit'),
        re_path(r'^wiki/delete/(?P<wiki_id>\d+)/$', wiki.wiki_delete, name='wiki_delete'),
        # wiki目录
        re_path(r'^wiki/catalog/$', wiki.wiki_catalog, name='wiki_catalog'),
        re_path(r'^wiki/uploads/$', wiki.wiki_uploads, name='wiki_uploads'),

        re_path(r'^file/$', file.file, name='file'),
        re_path(r'^file/delete/$', file.file_delete, name='file_delete'),
        re_path(r'^file/cos_credential/$', file.cos_credential, name='cos_credential'),
        re_path(r'^file/post/$', file.file_post, name='file_post'),
        re_path(r'^file/download/(?P<file_id>\d+)/$', file.file_download, name='file_download'),

        re_path(r'^setting/$', setting.setting, name='setting'),
        re_path(r'^setting/delete/$', setting.delete, name='setting_delete'),

        re_path(r'^issues/$', issues.issues, name='issues'),
        re_path(r'^issues/detail/(?P<issues_id>\d+)/$', issues.issues_detail, name='issues_detail'),
        re_path(r'^issues/record/(?P<issues_id>\d+)/$', issues.issues_record, name='issues_record'),
        re_path(r'^issues/change/(?P<issues_id>\d+)/$', issues.issues_change, name='issues_change'),
        re_path(r'^issues/invite/url/$', issues.issues_invite, name='issues_invite'),
        re_path(r'^issues_chart/$', dashboard.issues_chart, name='issues_chart'),
        re_path(r'^statistics/$', statistics.statistics, name='statistics'),
        re_path(r'^statistics_priority/$', statistics.statistics_priority, name='statistics_priority'),
        re_path(r'^statistics_project_user/$', statistics.statistics_project_user, name='statistics_project_user'),

    ], None), None),
    re_path(r'^invite_join/(?P<code>\w+)/$', issues.invite_join, name='invite_join'),
]
