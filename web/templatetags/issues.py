#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : issues.py
# @Author: WangXiang
# @Email : wxlinux@162.com
# @Date  : 2020/7/2 15:23
# @Tool  : PyCharm
# @Desc  :
from django.template import Library
from web import models

register = Library()


@register.simple_tag
def string_just(num):
    if num < 100:
        num = str(num).rjust(3, '0')

    return "#{}".format(num)


@register.simple_tag
def byte_convert(request):
    project_space = models.Transaction.objects.filter(
        user=request.tracer.user).first().price_policy.project_space
    use_space = request.tracer.project.use_space
    if use_space <= 4:
        use_space_msg = 'B'.format(use_space)
    elif 4 < use_space <= 8:
        use_space_msg = '{}KB'.format(round(use_space / 1024, 2))
    elif 8 < use_space <= 12:
        use_space_msg = '{}MB'.format(round(use_space / 1024 / 1024, 2))
    else:
        use_space_msg = '{}MB'.format(round(use_space / 1024 / 1024 / 1024, 2))

    return "{use_space_msg}/{project_space}GB".format(use_space_msg=use_space_msg, project_space=project_space)
