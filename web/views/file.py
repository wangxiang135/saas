#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''

@File  : file.py
@Author: WangXiang
@Email : wxlinux@162.com
@Date  : 2020/6/29 17:35
@Tool  : PyCharm
@Desc  : 
'''
import json
import requests

from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import HttpResponse
from django.http import JsonResponse
from django.urls import reverse

from django.views.decorators.csrf import csrf_exempt
from django.forms import model_to_dict

from web.forms.file import FolderModelForm
from web.forms.file import FileModelForm
from web import models

from utils.tencent.cos import delete_file
from utils.tencent.cos import delete_file_list
from utils.tencent.cos import credential


# http://127.0.0.1:8002/manage/1/file/
# http://127.0.0.1:8002/manage/1/file/?folder=1
def file(request, project_id):

    parent_object = None
    folder_id = request.GET.get('folder', "")
    if folder_id.isdecimal():
        parent_object = models.FileRepositoy.objects.filter(id=int(folder_id), project=request.tracer.project,
                                                            file_type=2).first()

    if request.method == "GET":
        # 导航条
        breadcrumb_list = []
        parent = parent_object
        while parent:
            breadcrumb_list.insert(0, model_to_dict(parent, ['id', 'name']))
            parent = parent.parent

        form = FolderModelForm(request, parent_object)
        queryset = models.FileRepositoy.objects.filter(project=request.tracer.project)
        if folder_id:
            file_object_list = queryset.filter(parent=parent_object).order_by('-file_type')
        else:
            file_object_list = queryset.filter(parent__isnull=True).order_by('-file_type')

        context = {'form': form, 'file_object_list': file_object_list, 'breadcrumb_list': breadcrumb_list,
                   'folder_object': parent_object}
        return render(request, 'file.html', context)

    # POST 创建文件夹 & 修改文件夹
    fid = request.POST.get('fid', "")
    # edit_object = None
    if fid.isdecimal():
        edit_object = models.FileRepositoy.objects.filter(id=int(fid), project=request.tracer.project,
                                                          file_type=2).first()
    edit_object = None

    if edit_object:
        form = FolderModelForm(request, parent_object, data=request.POST, instance=edit_object)
    else:
        form = FolderModelForm(request, parent_object, data=request.POST)
    if form.is_valid():
        # 成功创建
        form.instance.project = request.tracer.project
        form.instance.file_type = 2
        form.instance.update_user = request.tracer.user
        form.instance.parent = parent_object
        form.save()
        return JsonResponse({'status': True})
    else:
        return JsonResponse({'status': False, 'error': form.errors})


def file_delete(request, project_id):
    """删除文件和文件夹"""
    fid = request.GET.get('fid', "")

    # 删除数据库中的文件&文件夹（级联操作）
    delete_object = models.FileRepositoy.objects.filter(id=fid, project=request.tracer.project).first()

    bucket = request.tracer.project.bucket
    region = request.tracer.project.region

    if delete_object.file_type == 1:
        key = delete_object.key
        delete_file(
            bucket=bucket,
            region=region,
            key=key,
        )  # 删除文件（删除数据库，cos文件删除，项目已使用空间容量还回去）

        # 删除文件，将空间还给当前项目的已使用空间
        request.tracer.project.use_space -= delete_object.file_type
        request.tracer.project.save()

    else:
        # 删除文件夹（找到文件夹下的所有文件--》数据库删除，cos删除，项目已使用空间容量还回去）
        total_size = 0
        key_list = []  # 删除的文件key列表
        folder_list = [delete_object, ]
        for folder in folder_list:
            child_list = models.FileRepositoy.objects.filter(project=request.tracer.project, parent=folder).order_by(
                '-file_type')
            for child in child_list:
                if child.file_type == 2:
                    folder_list.append(folder_list)
                else:
                    # 文件大小相加
                    total_size += child.file_size

                    # 要删除文件
                    key_list.append({"Key": child.key})
        delete_file_list(bucket, region, key_list)

    # 删除文件
    delete_object.delete()

    return JsonResponse({'status': True})


@csrf_exempt
def cos_credential(request, project_id):
    """
    cos临时凭证&文件容量限制
    :param request:
    :param project_id:
    :return:
    """
    # request.body  ==》 b'[{"name":"1.jpg","size":46630}]'
    file_list = json.loads(request.body.decode('utf-8'))
    per_file_limit = request.tracer.price_policy.per_file_size * 1024 * 1024  # 单文件大小
    total_file_limit = request.tracer.price_policy.project_space * 1024 * 1024 * 1024

    total_size = 0
    for item in file_list:
        # 文件的字节大小 item['size'] = B
        # 单文件限制的大小 M
        # 超出限制
        if item['size'] > per_file_limit:
            msg = "单文件超出限制（最大{}M），文件：{}，请升级套餐。".format(request.tracer.price_policy.per_file_size, item['name'])
            return JsonResponse({'status': False, 'error': msg})
        total_size += item['size']
        # 做容量限制：单文件 & 总容量

    # 总容量进行限制
    # request.tracer.price_policy.project_space  # 项目的允许的空间
    # request.tracer.project.use_space # 项目已使用的空间
    if total_size + request.tracer.project.use_space > total_file_limit:
        return JsonResponse({'status': False, 'error': "容量超过限制，请升级套餐。"})

    data_dict = credential(bucket=request.tracer.project.bucket,
                           region=request.tracer.project.region,
                           )
    print(data_dict)
    return JsonResponse({'status': True, 'data': data_dict})


@csrf_exempt
def file_post(request, project_id):
    """ 已上传成功的文件写入到数据 """
    """
    name: fileName,
    key: key,
    file_size: fileSize,
    parent: CURRENT_FOLDER_ID,
    # etag: data.ETag,
    file_path: data.Location
    """
    # 根据key再去cos获取文件Etag和"db7c0d83e50474f934fd4ddf059406e5"
    # 把获取到的数据写入数据库即可
    form = FileModelForm(request, data=request.POST)
    if form.is_valid():
        # 通过ModelForm.save存储到数据库中的数据返回的isntance对象，无法通过get_xx_display获取choice的中文
        # form.instance.file_type = 1
        # form.update_user = request.tracer.user
        # instance = form.save() # 添加成功之后，获取到新添加的那个对象（instance.id,instance.name,instance.file_type,instace.get_file_type_display()

        # 校验通过：数据写入到数据库
        data_dict = form.cleaned_data
        data_dict.pop('etag')
        data_dict.update({'project': request.tracer.project, 'file_type': 1, 'update_user': request.tracer.user})
        instance = models.FileRepositoy.objects.create(**data_dict)

        # 项目的已使用空间：更新 (data_dict['file_size'])
        request.tracer.project.use_space += data_dict['file_size']
        request.tracer.project.save()

        result = {
            'id': instance.id,
            'name': instance.name,
            'file_size': instance.file_size,
            'username': instance.update_user.username,
            'datetime': instance.update_datetime.strftime('%Y{y}%m{m}%d{d} %H:%M').format(y='年', m='月', d='日'),
            'file_path': instance.file_path,
            'download': reverse('file_download', kwargs={'project_id': instance.project.id, 'file_id': instance.id}),
            # 'file_type': instance.get_file_type_display()
        }
        return JsonResponse({'status': True, 'data': result})

    return JsonResponse({'status': False, 'error': "文件错误"})


def file_download(request, project_id, file_id):
    """ 下载文件 """
    file_object = models.FileRepositoy.objects.filter(project=request.tracer.project,
                                                      file_type=1,
                                                      id=file_id).first()

    #获取web内容
    res = requests.get(file_object.file_path)

    # 文件分块处理（适用于大文件）
    data = res.iter_content()

    # 设置content_type=application/octet-stream 用于提示下载框
    response = HttpResponse(data,content_type="application/octet-stream")
    from django.utils.encoding import escape_uri_path

    # 设置响应头：中文件文件名转义
    response['Content-Disposition'] = "attachment; filename={};".format(escape_uri_path(file_object.name))
    return response
