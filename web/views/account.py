"""
用户账号相关功能： 注册，登录，注销，短信
"""
import datetime
import uuid

from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.shortcuts import HttpResponse

import random
from utils.tencent.sms import send_sms_single
from django.conf import settings
from web import models

from web.forms.account import RegisterModelForm
from web.forms.account import SendSmsForm
from web.forms.account import LoginSmsForm
from web.forms.account import LoginForm

"""生成图片验证码所需要的库"""
from io import BytesIO
from utils.image_code import check_code


def send_sms(request):
    """发送短信"""

    form = SendSmsForm(request, data=request.POST)
    if form.is_valid():
        return JsonResponse({'status': True})
    return JsonResponse({'status': False, 'error': form.errors})


def image_code(request):
    """生成图片验证码"""

    # 创建带有image部定义的HttpResponse对象
    response = HttpResponse(content_type='image/png')

    image_object, code = check_code()

    # 设置session验证不超过60s
    request.session['image_code'] = code
    request.session.set_expiry(60)

    stream = BytesIO()
    image_object.save(stream, 'png')

    response.write(stream.getvalue())
    return response


def login(request):
    """密码登录+图片验证"""
    if request.method == 'GET':
        form = LoginForm(request)
        return render(request, 'login.html', {'form': form})

    form = LoginForm(request, data=request.POST)
    if form.is_valid():
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user_object = models.UserInfo.objects.filter(Q(mobile_phone=username) | Q(email=username)).filter(
            password=password).first()

        if user_object:
            # 用户名密码正确
            request.session['user_id'] = user_object.id
            request.session.set_expiry(60 * 60 * 24 * 14)
            return redirect('index')

        form.add_error('username', '用户名或密码错误')

    return render(request, 'login.html', {'form': form})


def logout(request):
    """注册登录"""
    request.session.flush()
    return redirect('index')


def login_sms(request):
    """短信登录"""
    if request.method == "GET":
        form = LoginSmsForm()
        return render(request, 'login_sms.html', {'form': form})

    form = LoginSmsForm(request.POST)
    if form.is_valid():
        # 用户输入正确，登录
        user_object = form.cleaned_data['mobile_phone']
        # 用户登录成功放入session
        request.session['user_id'] = user_object.id
        request.session.set_expiry(60 * 60 * 24 * 14)
        return JsonResponse({'status': True, 'data': '/index/'})
    return JsonResponse({'status': False, 'error': form.errors})


def register(request):
    if request.method == 'GET':
        form = RegisterModelForm()
        return render(request, 'register.html', {"form": form})

    form = RegisterModelForm(data=request.POST)
    if form.is_valid():
        # 验证通过，写入数据库（密码要是密文）
        # instance = form.save，在数据库中新增一条数据，并将新增的这条数据赋值给instance

        # 用户表中新建一条数据（注册）
        instance = form.save()

        # 创建交易记录
        # 方式一
        policy_object = models.PricePolicy.objects.filter(category=1, title="个人免费版").first()

        models.Transaction.objects.create(
            status=2,
            order=str(uuid.uuid4()),
            user=instance,
            price_policy=policy_object,
            count=0,
            price=0,
            start_datetime=datetime.datetime.now()
        )

        return JsonResponse({'status': True, 'data': '/login/'})

    return JsonResponse({'status': False, 'error': form.errors})
