#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : issues.py
# @Author: WangXiang
# @Email : wxlinux@162.com
# @Date  : 2020/7/1 16:56
# @Tool  : PyCharm
# @Desc  :
import json
import datetime

from django.shortcuts import render
from django.http import JsonResponse
from django.urls import reverse
from django.utils.safestring import mark_safe

from web import models
from django.views.decorators.csrf import csrf_exempt

from utils.encrypt import uid
from utils.pagination import Pagination
from web.forms.issues import IssuesReplyModelForm, InviteModelForm
from web.forms.issues import IssuesModelForm


class SelectFilter(object):
    def __init__(self, name, data_list, request):
        self.name = name
        self.data_list = data_list  # project_total_user（1，“哈哈”）
        self.request = request

    def __iter__(self):
        # http://127.0.0.1:8000/manage/6/issues/?assign=1?issues_type=1&issues_type=2
        # print(request.path_info, request.get_host(), request.get_raw_uri())
        # /manage/5/issues/ 127.0.0.1:8000 http://127.0.0.1:8000/manage/5/issues/?issues_type=1&issues_type=2

        yield mark_safe("<select class='select2' multiple='multiple' style='width:100%;'>")
        for key, value in self.data_list:
            # 默认选中
            key = str(key)
            selected = ''
            value_list = self.request.GET.getlist(self.name)

            if str(key) in value_list:
                selected = "selected"
                value_list.remove(key)
            else:
                value_list.append(key)

            # 为自己生成URL
            # 在当前URL的基础上去增加一项
            # status=1&age=19
            query_dict = self.request.GET.copy()  # request不允许修改，通过copy可以复制一份进行修改
            query_dict._mutable = True  # 允许修改值
            query_dict.setlist(self.name, value_list)
            if 'page' in query_dict:
                query_dict.pop('page')  # 踢出分页内容

            param_url = query_dict.urlencode()  # 如果里面值为name=status，value=1那么返回&status=1
            if param_url:  # 有&status=1值
                # url = "{scheme}://{host_port}{path_info}?{param_url}".format(scheme=self.request.scheme,
                #                        host_port=self.request.get_host(),
                #                        path_info=self.request.path_info,
                #                        param_url=param_url)  # status=1&status=2&status=3&xx=1

                url = "?{param_url}".format(param_url=param_url)  # status=1&status=2&status=3&xx=1
            else:
                url = self.request.path_info  # 设置为/manage/5/issues/

            html = "<option value='{url}' {selected}>{text}</option>"
            html = html.format(url=url, selected=selected, text=value)
            yield mark_safe(html)

        yield mark_safe("</select>")


class CheckFilter(object):
    def __init__(self, name, data_list, request):
        self.name = name
        self.data_list = data_list
        self.request = request

    def __iter__(self):
        for item in self.data_list:
            key = str(item[0])
            text = item[1]
            ck = ""
            # 如果当前用户请求的URL中status和当前循环key相等
            value_list = self.request.GET.getlist(self.name)
            if key in value_list:
                ck = 'checked'
                value_list.remove(key)
            else:
                value_list.append(key)

            # 为自己生成URL
            # 在当前URL的基础上去增加一项
            # status=1&age=19
            from django.http import QueryDict
            query_dict = self.request.GET.copy()
            query_dict._mutable = True
            query_dict.setlist(self.name, value_list)
            if 'page' in query_dict:
                query_dict.pop('page')

            param_url = query_dict.urlencode()
            if param_url:
                url = "{}?{}".format(self.request.path_info, param_url)  # status=1&status=2&status=3&xx=1
            else:
                url = self.request.path_info

            tpl = '<a class="cell" href="{url}"><input type="checkbox" {ck} /><label>{text}</label></a>'
            html = tpl.format(url=url, ck=ck, text=text)
            yield mark_safe(html)


def issues(request, project_id):
    # 根据URL做筛选，筛选条件（根据用户通过GET传过来的参数实现）
    # ?status=1&status=2&issues_type=1
    allow_filter_name = ['issues_type', 'status', 'priority', 'assign', 'attention']
    condition = {}
    for name in allow_filter_name:
        value_list = request.GET.getlist(name)  # [1,2]
        if not value_list:
            continue
        condition['{}__in'.format(name)] = value_list
    """
            condition = {
                "status__in":[1,2],
                'issues_type':[1,]
            }
    """
    if request.method == 'GET':
        # 分页获取数据
        queryset = models.Issues.objects.filter(project_id=project_id).filter(**condition)
        page_object = Pagination(
            current_page=request.GET.get('page'),
            all_count=queryset.count(),
            base_url=request.path_info,
            query_params=request.GET,
            per_page=50,
        )
        issues_object_list = queryset[page_object.start:page_object.end]
        form = IssuesModelForm(request)

        project_issues_type = models.IssuesType.objects.filter(project_id=project_id).values_list('id', 'title')

        project_total_user = [(request.tracer.project.creator_id, request.tracer.project.creator.username,)]
        join_user = models.ProjectUser.objects.filter(project_id=project_id).values_list('user_id', 'user__username')
        project_total_user.extend(join_user)

        invite_form = InviteModelForm()
        centext = {
            'form': form,
            'invite_form': invite_form,
            'issues_object_list': issues_object_list,
            'page_html': page_object.page_html(),
            'filter_list': [
                {'title': "问题类型", 'filter': CheckFilter('issues_type', project_issues_type, request)},
                {'title': "状态", 'filter': CheckFilter('status', models.Issues.status_choices, request)},
                {'title': "优先级", 'filter': CheckFilter('priority', models.Issues.priority_choices, request)},
                {'title': "指派者", 'filter': SelectFilter('assign', project_total_user, request)},
                {'title': "关注者", 'filter': SelectFilter('attention', project_total_user, request)},
            ]
        }

        return render(request, 'issues.html', centext)

    form = IssuesModelForm(request, data=request.POST)
    if form.is_valid():
        form.instance.project = request.tracer.project
        form.instance.creator = request.tracer.user
        form.save()
        # context = {
        #     'form': form,
        #     'id':instance.id,
        #     'issues_type':instance.issues_type,
        #
        # }
        return JsonResponse({'status': True})

    return JsonResponse({'status': False, 'error': form.errors})


def issues_detail(request, project_id, issues_id):
    """编辑问题"""
    issues_object = models.Issues.objects.filter(project_id=project_id, id=issues_id).first()
    form = IssuesModelForm(request, instance=issues_object)
    return render(request, 'issues_detail.html', {'form': form, 'issues_object': issues_object})


@csrf_exempt
def issues_record(request, project_id, issues_id):
    """初始化操作记录"""

    if request.method == 'GET':
        reply_object = models.IssuesReply.objects.filter(issues_id=issues_id, issues__project=request.tracer.project)
        # 将queryset转换为json格式
        data_list = []
        for row in reply_object:
            data = {
                'id': row.id,
                'reply_type_text': row.get_reply_type_display(),
                'content': row.content,
                'creator': row.creator.username,
                'datetime': row.create_datetime.strftime('%Y{y}%m{m}%d{d} %H:%M').format(y='年', m='月', d='日'),
                'parent_id': row.reply_id,
            }
            data_list.append(data)

        return JsonResponse({'status': True, 'data': data_list})

    form = IssuesReplyModelForm(data=request.POST)
    if form.is_valid():
        form.instance.reply_type = 2
        form.instance.issues_id = issues_id
        form.instance.creator = request.tracer.user
        instance = form.save()
        info = {
            'id': instance.id,
            'reply_type_text': instance.get_reply_type_display(),
            'content': instance.content,
            'creator': instance.creator.username,
            'datetime': instance.create_datetime.strftime('%Y{y}%m{m}%d{d} %H:%M').format(y='年', m='月', d='日'),
            'parent_id': instance.reply_id,
        }
        return JsonResponse({'status': True, 'data': info})
    else:
        return JsonResponse({'status': False, 'error': form.errors})


@csrf_exempt
def issues_change(request, project_id, issues_id):
    """修改issues"""
    # print(request.POST)   < QueryDict: {'{"name":"subject","value":"哈哈dada"}': ['']} >
    # print(request.body.decode('utf-8'))   {"name": "subject", "value": "哈哈dada"}
    # {"name":"attention","value":["3"]}

    issues_object = models.Issues.objects.filter(project_id=project_id, id=issues_id).first()
    post_dict = json.loads(request.body.decode('utf-8'))
    name = post_dict.get('name')
    value = post_dict.get('value')
    '''
    根据用户返回 {"name": "subject", "value": "哈哈dada"} 内容更新数据库字段
    models.Issues._meta.get_field(name)         <class 'django.db.models.fields.CharField'> web.Issues.subject
    '''
    field_object = models.Issues._meta.get_field(name)

    def create_reply_record(content):
        new_object = models.IssuesReply.objects.create(
            reply_type=1,
            issues=issues_object,
            content=change_record,
            creator=request.tracer.user,
        )

        new_reply_dict = {
            'id': new_object.id,
            'reply_type_text': new_object.get_reply_type_display(),
            'content': new_object.content,
            'creator': new_object.creator.username,
            'datetime': new_object.create_datetime.strftime('%Y{y}%m{m}%d{d} %H:%M').format(y='年', m='月', d='日'),
            'parent_id': new_object.reply_id,
        }
        return new_reply_dict

    # 1.数据库字段更新
    # 1.1文本
    text_list = ['subject', 'desc', 'start_date', 'end_date']
    if name in text_list:
        # 判断 value有没有传值进来，和数据库字段可不可以为空
        if not value:
            # 无值
            # field_object.null 获取models字段可不可是空
            if not field_object.null:
                # models.Issues._meta.verbose_name   models.SmallIntegerField(verbose_name='模式'
                return JsonResponse({'status': False, 'error': "你选择的值不能为空"})
            getattr(issues_object, name, None)  # issues_object.subject=None
            issues_object.save()
            change_record = "{}更新为空".format(field_object.verbose_name)
        else:
            getattr(issues_object, name, value)  # issues_object.subject="哈哈哈"
            issues_object.save()
            change_record = "{}更新为{}".format(field_object.verbose_name, value)

        return JsonResponse({'status': True, 'data': create_reply_record(change_record)})

    # 1.2 FK字段（指派的话要判断是否创建者或参与者）
    if name in ['issues_type', 'module', 'parent', 'assign']:
        # 用户选择为空
        if not value:
            if not field_object.null:
                return JsonResponse({'status': False, 'error': "你选择的值不能为空"})
            # 允许为空
            setattr(issues_object, name, None)
            issues_object.save()
            change_record = "{}更新为空".format(field_object.verbose_name)
        else:
            if name == 'assign':
                # 是否是创建者
                if value == str(request.tracer.project.creator_id):
                    instance = request.tracer.project.creator
                else:
                    project_user_object = models.ProjectUser.objects.filter(project_id=project_id,
                                                                            user_id=value).first()
                    if project_user_object:
                        instance = project_user_object.user
                    else:
                        instance = None
                if not instance:
                    return JsonResponse({'status': False, 'error': "你选择的值不存在"})

                setattr(issues_object, name, value)
                issues_object.save()
                change_record = "{}更新为{}".format(field_object.verbose_name, str(instance))  # value根据文本获取到内容

            else:
                # 条件判断：用户输入的值，是自己的值
                instance = field_object.remote_field.model.objects.filter(id=value, project_id=project_id).first()
                """
                Django 2.0 与 Django1.0在内部方法上的差异
                models.Customer.tags.rel 方法
                对于Django1 可以通过 field_object.rel.model.objects.all() 获取一个model下的ManytoMany字段下的所有内容；
                
                在Django2 中rel下的to方法被去掉了，直接调用to方法会报 'ManyToManyRel' object has no attribute 'to' 错误，在Django2 中可以用rel下的model方法实现相同的功能，如：
                field_object.rel.model.objects.all()
                """

                if not instance:
                    return JsonResponse({'status': False, 'error': "你选择的值不存在"})

                setattr(issues_object, name, instance)
                issues_object.save()
                change_record = "{}更新为{}".format(field_object.verbose_name, str(instance))

        return JsonResponse({'status': True, 'data': create_reply_record(change_record)})

    # 1.3choices字段
    if name in ['priority', 'status', 'mode']:
        select_text = None
        for key, text in field_object.choices:
            if str(key) == value:
                select_text = text

        if not select_text:
            return JsonResponse({'status': False, 'error': "你选择的值不存在"})

        setattr(issues_object, name, value)
        issues_object.save()
        change_record = "{}更新为{}".format(field_object.verbose_name, select_text)
        return JsonResponse({'status': True, 'data': create_reply_record(change_record)})

    # 1.4M2M字段
    if name == "attention":
        # {"name":"attention","value":[1,2,3]}
        if not isinstance(value, list):
            return JsonResponse({'status': False, 'error': "数据格式错误"})

        if not value:
            issues_object.attention.set(value)
            issues_object.save()
            change_record = "{}更新为空".format(field_object.verbose_name)
        else:
            # values=["1","2,3,4]  ->   id是否是项目成员（参与者、创建者）
            # 获取当前项目的所有成员
            user_dict = {str(request.tracer.project.creator_id): request.tracer.project.creator.username}
            project_user_list = models.ProjectUser.objects.filter(project_id=project_id)
            for item in project_user_list:
                user_dict[str(item.user_id)] = item.user.username
            username_list = []
            for user_id in value:
                username = user_dict.get(str(user_id))
                if not username:
                    return JsonResponse({'status': False, 'error': "用户不存在请重新设置"})
                username_list.append(username)

            issues_object.attention.set(value)
            issues_object.save()
            change_record = "{}更新为{}".format(field_object.verbose_name, ",".join(username_list))

        return JsonResponse({'status': True, 'data': create_reply_record(change_record)})

    return JsonResponse({'status': False, 'error': "滚"})


def issues_invite(request, project_id):
    """生成邀请码"""
    form = InviteModelForm(data=request.POST)
    if form.is_valid():
        """
        1.创建随机验证码
        2.验证保存到数据库
        3.限制：只有创建者才能邀请
        """
        if request.tracer.project.creator != request.tracer.user:
            form.add_error('period', "无权创建邀请码")
            return JsonResponse({'status': False, 'error': form.errors})
        random_invite_code = uid(request.tracer.user.mobile_phone)
        form.instance.project = request.tracer.project
        form.instance.creator = request.tracer.user
        form.instance.code = random_invite_code
        form.save()

        # 将验证码返回给客户端
        url = "{scheme}://{host}{path}".format(
            scheme=request.scheme,
            host=request.get_host(),
            path=reverse('invite_join', kwargs={'code': random_invite_code})
        )

    return JsonResponse({'status': True, 'data': url})


def invite_join(requset, code):
    """访问验证码"""

    invite_object = models.ProjectInvite.objects.filter(code=code).first()
    if not invite_object:
        return render(requset, 'invite_join.html', {'error': '邀请码不存在'})

    if invite_object.creator == requset.tracer.user:
        return render(requset, 'invite_join.html', {'error': '创建者无需再加入项目'})

    exists = models.ProjectUser.objects.filter(user=requset.tracer.user).exists()
    if exists:
        return render(requset, 'invite_join.html', {'error': '你已经在项目中无需重复加入'})

    # 邀请码是否过期
    current_datetime = datetime.datetime.now()
    limit_datetime = invite_object.create_datetime + datetime.timedelta(minutes=invite_object.period)
    if current_datetime > limit_datetime:
        return render(requset, 'invite_join.html', {'error': '邀请码过期'})

    # 是否已经过期, 如果已过期使用免费额度
    max_transaction = ""
    max_transaction = models.Transaction.objects.filter(user=invite_object.project.creator).order_by('-id').first()
    if max_transaction.price_policy.category == 1:
        max_member = max_transaction.price_policy.project_member
    else:
        if max_transaction.end_datetime < current_datetime:
            free_object = models.PricePolicy.objects.filter(category=1).first()
            max_member = free_object.project_member
        else:
            max_member = max_transaction.price_policy.project_member


    # 最多允许的成员
    # max_member = models.Transaction.objects.filter(user=requset.tracer.user).price_policy.project_member

    # 目前所有成员（创建者&参与者）
    current_member = models.ProjectUser.objects.filter(project=requset.tracer.project).count()
    current_member = current_member + 1
    if current_member >= max_member:
        return render(requset, 'invite_join.html', {'error': '项目成员超过限制，请购买套餐'})

    # 数量限制
    if invite_object.count:
        return render(requset, 'invite_join.html', {'error': '邀请码数量已经用完'})
    invite_object.use_count += 1
    invite_object.save()

    models.ProjectUser.objects.create(user=requset.tracer.user, project=requset.tracer.project)
    return render(requset, 'invite_join.html', {'project': invite_object.project})
