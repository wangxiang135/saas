#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : dashboard.py
# @Author: WangXiang
# @Email : wxlinux@162.com
# @Date  : 2020/7/4 17:04
# @Tool  : PyCharm
# @Desc  :
import collections
import datetime
import time

from django.db.models import Count
from django.shortcuts import render
from django.http import JsonResponse

from web import models


def dashboard(request, project_id):
    # 问题显示面板  .values分组 annotate查询集
    issues_data = models.Issues.objects.filter(project_id=project_id).values('status').annotate(ct=Count('id'))
    # <QuerySet [{'status': 1, 'ct': 3}, {'status': 5, 'ct': 1}]>

    # 有序字典
    status_dict = collections.OrderedDict()
    for key, text in models.Issues.status_choices:
        status_dict[key] = {"text": text, 'count': 0}

    for item in issues_data:
        key = item["status"]
        count = item["ct"]
        status_dict[key]['count'] = count

    # 项目参与者
    user_list = models.ProjectUser.objects.filter(project_id=project_id).values('user_id', 'user__username')

    # 动态
    top_ten_object = models.Issues.objects.filter(project_id=project_id).order_by('-id')[0:10]

    context = {
        'status_dict': status_dict,
        'user_list': user_list,
        'top_ten_object': top_ten_object,
    }

    return render(request, 'dashboard.html', context)


def issues_chart(request, project_id):
    '''在橄榄页面生产图表所需数据'''

    today = datetime.datetime.now().date()
    # datetime.datetime.now() 2020-07-06 15:38:47.483793
    # datetime.datetime.now().date()  2020-07-06

    # 生成30天数据
    date_dict = collections.OrderedDict()
    for i in range(30):
        date = today - datetime.timedelta(days=i)
        date_dict[date.strftime("%Y-%m-%d")] = [time.mktime(date.timetuple()) * 1000, 0]

    result = models.Issues.objects.filter(project_id=project_id,
                                          create_datetime__gte=today - datetime.timedelta(days=30)).extra(
        select={'ctime': "strftime('%%Y-%%m-%%d',web_issues.create_datetime)"}).values('ctime').annotate(ct=Count('id'))
    # <QuerySet [{'ctime': '2020-07-02', 'ct': 5}, {'ctime': '2020-07-03', 'ct': 1}]>
    for item in result:
        date_dict[item['ctime']][1] = item['ct']

    return JsonResponse({'status': True, 'data': list(date_dict.values())})
