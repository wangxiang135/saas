from django.http import JsonResponse
from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import HttpResponse
from django.shortcuts import reverse
from django.views.decorators.csrf import csrf_exempt

from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.clickjacking import xframe_options_deny
from django.views.decorators.clickjacking import xframe_options_sameorigin

from django.conf import settings
from web import models

from web.forms.project import ProjectModelForm
from web.forms.wiki import WikiModeForm
from utils.tencent.cos import upload_file
from utils.encrypt import uid


def wiki(request, project_id):
    """wiki"""
    wiki_id = request.GET.get('wiki_id')
    if wiki_id:
        wiki_object = models.Wiki.objects.filter(id=wiki_id, project=request.tracer.project).first()
        return render(request, 'wiki.html', {'wiki_object': wiki_object})

    return render(request, 'wiki.html')


def wiki_add(request, project_id):
    """wiki"""
    if request.method == "GET":
        form = WikiModeForm(request)
        return render(request, 'wiki_form.html', {'form': form})

    form = WikiModeForm(request, data=request.POST)
    if form.is_valid():
        form.instance.project = request.tracer.project
        if form.instance.parent:
            form.instance.depth = form.instance.parent.depth + 1
        else:
            form.instance.depth = 1
        form.save()
        url = reverse('wiki', kwargs={'project_id': project_id})
        # http://127.0.0.1:8000/manage/2/wiki/
        return redirect(url)
    return render(request, 'wiki_form.html', {'form': form})


def wiki_catalog(request, project_id):
    """wiki目录"""

    data = models.Wiki.objects.filter(project=request.tracer.project).values("id", 'title', 'parent_id').order_by(
        'depth', 'id')
    # print(type(data))
    return JsonResponse({'status': True, 'data': list(data)})


def wiki_edit(request, project_id, wiki_id):
    wiki_object = models.Wiki.objects.filter(id=wiki_id, project=project_id).first()
    if request.method == "GET":
        form = WikiModeForm(request, instance=wiki_object)
        return render(request, 'wiki_form.html', {'form': form})

    form = WikiModeForm(request, data=request.POST, instance=wiki_object)
    if form.is_valid():
        if form.instance.parent:
            form.instance.depth = form.instance.parent.depth + 1
        else:
            form.instance.depth = 1
        form.save()
        url = reverse('wiki', kwargs={'project_id': project_id})
        preview_url = "{0}?wiki_id={1}".format(url, wiki_id)
        return redirect(preview_url)

    return render(request, 'wiki_form.html', {'form': form})


def wiki_delete(request, project_id, wiki_id):
    models.Wiki.objects.filter(id=wiki_id, project=request.tracer.project).first().delete()
    return redirect(reverse('wiki', kwargs={'project_id': project_id}))


@csrf_exempt
@xframe_options_exempt
def wiki_uploads(request, project_id):
    """
    X-Frame-Options: DENY     //表示该页面不允许在 frame 中展示，即便是在相同域名的页面中嵌套也不允许。。
    X-Frame-Options: SAMEORIGIN    //表示该页面可以在相同域名页面的 frame 中展示。
    X-Frame-Options: ALLOW-FROM http://caibaojian.com/   //表示该网页只能放在http://caibaojian.com//网页架设的iFrame内。
    :param request:
    :param project_id:
    :return:
    """
    # Django的X-Frame-Options设置
    result = {
        'success': 0,  # 0 | 1, //0表示上传失败;1表示上传成功
        'message': None,  # "提示的信息"
        'url': None  # 上传成功时才返回
    }
    image_object = request.FILES.get('editormd-image-file')
    if not image_object:
        result['message'] = "图片上传失败"
        return JsonResponse(result)

    name_ext = image_object.name.rsplit('.')[-1]  # .jpg  .png
    key = "{}.{}".format(uid(request.tracer.user.mobile_phone), name_ext)  # xxx.png
    image_url = upload_file(
        bucket=request.tracer.project.bucket,
        region=request.tracer.project.region,
        file_object=image_object,
        key=key,
    )
    print(image_url)
    result['success'] = 1
    result['url'] = image_url
    # {'success': 1, 'message': None, 'url': 'https://crm-1252310654.cos.ap-shanghai.myqcloud.com/f3059ce68d15666137daee695726e181.jpg'}
    return JsonResponse(result)
