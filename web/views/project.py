from time import time

from django.http import JsonResponse
from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import HttpResponse


from django.conf import settings
from web import models

from web.forms.project import ProjectModelForm

from utils.tencent.cos import create_bucket

def project_list(request):
    """项目列表"""

    if request.method == 'GET':
        # GET请求查看项目列表
        """
        1. 从数据库中获取两部分数据
            我创建的所有项目：已星标、未星标
            我参与的所有项目：已星标、未星标
        2. 提取已星标
            列表 = 循环 [我创建的所有项目] + [我参与的所有项目] 把已星标的数据提取

        得到三个列表：星标、创建、参与
        """
        project_dict = {'star':[],'my':[],'join':[]}

        my_project_list = models.Project.objects.filter(creator=request.tracer.user)
        for my_project in my_project_list:
            if my_project.star:
                project_dict['star'].append({"value": my_project, 'type': 'my'})
            else:
                project_dict['my'].append({"value": my_project, 'type': 'my'})

        join_project_list = models.ProjectUser.objects.filter(user=request.tracer.user)
        for join_project in join_project_list:
            if join_project.star:
                project_dict['star'].append({"value": join_project.project, 'type': 'join'})
            else:
                project_dict['join'].append({"value": join_project.project, 'type': 'join'})


        form = ProjectModelForm(request)
        return render(request,'project_list.html',{'form':form, 'project_dict':project_dict})

    form = ProjectModelForm(request,data=request.POST)
    if form.is_valid():
        form.instance.creator = request.tracer.user
        #验证通过： 项目名，颜色，描述，创建者
        #创建项目
        #创建cos桶
        bucket = "{}-{}-{}".format(request.tracer.user.mobile_phone,str(int(time())),settings.TENCENT_COS_APPID) #cos存储桶名字
        region = "ap-shanghai"  #cos存储区域
        create_bucket(bucket,region)
        form.instance.bucket = bucket
        form.instance.region = region
        instance = form.save()

        #创建项目自动生成 issuesType默认类型
        # 任务，功能，Bug
        issues_type_object_list = []
        for item in models.IssuesType.PROJECT_INIT_LIST:
            issues_type_object_list.append(models.IssuesType(project=instance,title=item))
        models.IssuesType.objects.bulk_create(issues_type_object_list)

        return JsonResponse({'status': True})
    return JsonResponse({'status':False,'error':form.errors})

def project_star(request,project_type,project_id):
    if project_type == 'my':
        models.Project.objects.filter(creator=request.tracer.user,id=project_id).update(star=True)
        return redirect('project_list')

    if project_type == 'join':
        models.ProjectUser.objects.filter(user=request.tracer.user,project_id=project_id).update(star=True)
        return redirect('project_list')

    return HttpResponse('请求错误')

def project_unstar(request,project_type,project_id):
    if project_type == 'my':
        models.Project.objects.filter(creator=request.tracer.user,id=project_id).update(star=False)
        return redirect('project_list')

    if project_type == 'join':
        models.ProjectUser.objects.filter(user=request.tracer.user,project_id=project_id).update(star=False)
        return redirect('project_list')

    return HttpResponse('请求错误')
