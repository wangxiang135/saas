#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''

@File  : manage.py
@Author: WangXiang
@Email : wxlinux@162.com
@Date  : 2020/6/23 14:38
@Tool  : PyCharm
@Desc  : 
'''

from django.http import JsonResponse
from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import HttpResponse


from django.conf import settings
from web import models


def dashboard(request,project_id):
    return render(request,'dashboard.html')

def issues(request,project_id):
    return render(request,'issues.html')

def statistics(request,project_id):
    return render(request,'statistics.html')

def file(request,project_id):
    return render(request,'file.html')

def wiki(request,project_id):

    return render(request,'wiki.html')

def setting(request,project_id):
    return render(request,'setting.html')