from django import forms
from web import models
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.conf import settings
from utils.tencent.sms import send_sms_single


from django_redis import get_redis_connection
import random
from utils import encrypt
from web.forms.bootstrap import BootStarpForm

from web.forms.widgets import ColorRadioSelect



class ProjectModelForm(BootStarpForm,forms.ModelForm):
    bootstarp_class_exclude = ['color',]

    class Meta:
        model = models.Project
        fields = ['name','color','desc']
        widgets = {
            'desc':forms.Textarea(),
            'color':ColorRadioSelect(attrs={'class':'color-radio'}),
        }

    def __init__(self,request,*args,**kwargs):
        super(ProjectModelForm, self).__init__(*args,**kwargs)
        self.request = request

    def clean_name(self):
        """项目效验"""
        name = self.cleaned_data['name']
        # 1. 当前用户是否已创建过此项目(项目名是否已存在)？
        exists = models.Project.objects.filter(name=name,creator=self.request.tracer.user).exists()
        if exists:
            raise ValidationError('项目名已经存在')

        # 2. 当前用户是否还有额度进行创建项目？
        # 最多创建N个项目
        # self.request.tracer.price_policy.project_num
        count = models.Project.objects.filter(creator=self.request.tracer.user).count()
        if count >= self.request.tracer.price_policy.project_num:
            raise ValidationError('项目个数超限，请购买套餐')
        # 现在已创建多少项目？

        return name

