#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''

@File  : issues.py
@Author: WangXiang
@Email : wxlinux@162.com
@Date  : 2020/7/1 17:24
@Tool  : PyCharm
@Desc  : 
'''
from django import forms
from django.forms import ModelForm
from web.forms.bootstrap import BootStarpForm
from web import models


class IssuesModelForm(BootStarpForm, ModelForm):
    class Meta:
        model = models.Issues
        exclude = ['project', 'creator', 'create_datetime', 'latest_update_datetime']
        widgets = {
            "issues_type": forms.Select(attrs={'class': "selectpicker", "data-live-search": "true"}),
            "mode": forms.Select(attrs={'class': "selectpicker", "data-live-search": "true"}),
            "module": forms.Select(attrs={'class': "selectpicker", "data-live-search": "true"}),
            'priority': forms.Select(attrs={'class': "selectpicker", "data-live-search": "true"}),
            'status': forms.Select(attrs={'class': "selectpicker", "data-live-search": "true"}),
            'assign': forms.Select(attrs={'class': "selectpicker", "data-live-search": "true"}),
            'attention': forms.SelectMultiple(
                attrs={'class': "selectpicker", "data-live-search": "true", "data-actions-box": "true"}),
            # 多选
            "parent": forms.Select(attrs={'class': "selectpicker", "data-live-search": "true"}),
            'start_date': forms.DateTimeInput(attrs={'autocomplete': "off"}),
            'end_date': forms.DateTimeInput(attrs={'autocomplete': 'off'}),  # date插件

        }

    def __init__(self, request, *args, **kwargs):
        super(IssuesModelForm, self).__init__(*args, **kwargs)

        # 当前数据初始化
        # 1.获取当前项目的问题类型[(1,'xx'),(2,'xx')]
        self.fields['issues_type'].choices = models.IssuesType.objects.filter(
            project=request.tracer.project).values_list('id', 'title')

        # 2.获取当前项目的所有模块
        module_list = [('', "没有选中任何项")]
        module_object_list = models.Module.objects.filter(project=request.tracer.project).values_list('id',
                                                                                                      'title')
        module_list.extend(module_object_list)
        self.fields['module'].choices = module_list

        # 3.指派和关注者
        # 数据库找到当前项目的参与者 和 创建者
        total_user_list = [(request.tracer.project.creator.id, request.tracer.project.creator.username)]
        project_user_object_list = models.ProjectUser.objects.filter(
            project=request.tracer.project).values_list('user_id', 'user__username')
        total_user_list.extend(project_user_object_list)
        self.fields['assign'].choices = total_user_list
        self.fields['attention'].choices = [('', '没有选中任何项')] + total_user_list

        # 4. 当前项目已创建的问题
        parent_list = [('', '没有选中任何项')]
        parent_object_list = models.Issues.objects.filter(project=request.tracer.project).values_list('id', 'subject')
        parent_list.extend(parent_object_list)
        self.fields['parent'].choices = parent_list


class IssuesReplyModelForm(ModelForm):
    class Meta:
        model = models.IssuesReply
        fields = ['content', 'reply']


class InviteModelForm(BootStarpForm, ModelForm):
    class Meta:
        model = models.ProjectInvite
        fields = ['period', 'count']
