#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''

@File  : wiki.py
@Author: WangXiang
@Email : wxlinux@162.com
@Date  : 2020/6/28 15:47
@Tool  : PyCharm
@Desc  : 
'''

from django import forms
from django.forms import ModelForm
from web import models

from web.forms.bootstrap import BootStarpForm

class WikiModeForm(BootStarpForm,ModelForm):
    bootstarp_class_exclude = ['project','depth']

    class Meta:
        model = models.Wiki
        fields = ['id','title','content','parent']



    def __init__(self,request,*args,**kwargs):
        super(WikiModeForm, self).__init__(*args,**kwargs)
        total_data_list = [("","请选择")]
        data_list = models.Wiki.objects.filter(project=request.tracer.project).values_list('id','title')
        total_data_list.extend(data_list)
        self.fields['parent'].choices = total_data_list


