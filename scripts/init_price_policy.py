import base
from web import models


def run1():
    exists = models.PricePolicy.objects.filter(category=1, title="免费版").exists()
    if not exists:
        models.PricePolicy.objects.create(
            category=1,
            title="个人免费版",
            price=0,
            project_num=3,
            project_member=2,
            project_space=20,
            per_file_size=5,
        )


def run2():
    models.PricePolicy.objects.create(
        title="VIP",
        price=100,
        project_num=50,
        project_member=10,
        project_space=10,
        per_file_size=500,
        category=2
    )
    models.PricePolicy.objects.create(
        title="SVIP",
        price=200,
        project_num=150,
        project_member=110,
        project_space=110,
        per_file_size=1024,
        category=2
    )

    models.PricePolicy.objects.create(
        title="SVIP",
        price=500,
        project_num=300,
        project_member=300,
        project_space=300,
        per_file_size=2048,
        category=2
    )
if __name__ == '__main__':
    run2()
